package com.ifmo.ctddev.zaraijskiy;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.function.BiFunction;

/**
 * This class implements the segment tree with generic parameter.
 *
 * @param <T> is a Generic parameter
 * @author Zaraijskiy Alexsey
 * @version 1.0
 */
public class SegmentTree<T> {
	private T[] values;
	private final BiFunction<T, T, T> function;
	private final T neutralElement;
	private final int lengthInputData;

	/**
	 * Constructs a segment tree that contains input array, function and neutral element of monoid.
	 *
	 * @param values         is input array with Generic type T
	 * @param function       is function of monoid
	 * @param neutralElement is neutral element of monoid
	 * @throws NullPointerException       if array or function or neutral element is null
	 * @throws IllegalArgumentException   if input array is empty or size of input array more that 2^30
	 */
	public SegmentTree(T[] values,
	                   BiFunction<T, T, T> function,
	                   T neutralElement) {
		if (values == null) {
			throw new NullPointerException("values = null");
		}
		if (function == null) {
			throw new NullPointerException("function = null");
		}
		if (neutralElement == null) {
			throw new NullPointerException("neutralElement = null");
		}
		if (values.length == 0) {
			throw new IllegalArgumentException("Input array is empty");
		}
		if (values.length > 1 << 30) {
			throw new IllegalArgumentException("Max size = 2^30, but current size of input array : " + values.length);
		}
		this.function = function;
		this.neutralElement = neutralElement;
		this.lengthInputData = values.length;
		this.values = makeTree(values, neutralElement);
	}

	/**
	 * This method creates a new array based on input array and neutral elements.
	 * On this array is implemented segment tree.
	 *
	 * @param origin         is input array
	 * @param neutralElement is neutral element of monoid
	 * @return new array for segment tree
	 */
	private T[] makeTree(T[] origin, T neutralElement) {
		int validOriginSize = getValidSize(origin.length);
		int newSize = (2 * validOriginSize) - 1;
		@SuppressWarnings("unchecked")
		T[] ts = (T[]) Array.newInstance(origin.getClass().getComponentType(), newSize);
		Arrays.fill(ts, neutralElement);
		System.arraycopy(origin, 0, ts, validOriginSize - 1, origin.length);
		for (int i = (ts.length - 1) / 2 - 1; i >= 0; i--) {
			ts[i] = function.apply(ts[2 * i + 1], ts[2 * i + 2]);
		}
		return ts;
	}

	/**
	 * This method updates the tree starting from the element-leaf, and finishing at the root.
	 *
	 * @param index is index of the element which is located in the array values
	 */
	private void updateTree(int index) {
		while (index != 0) {
			index = (index - 1) / 2;
			values[index] = function.apply(values[index * 2 + 1], values[index * 2 + 2]);
		}
	}

	/**
	 * This method return count >= size and count = 2^k
	 *
	 * @param size is input size
	 * @return count >= size and count = 2^k
	 */
	private int getValidSize(int size) {
		for (int mask = 1 << 30; mask != 0; mask >>>= 1) {
			if ((size & mask) != 0) {
				return (mask != size) ? mask << 1 : mask;
			}
		}
		return -1;
	}

	/**
	 * This method calculate answer of input array from {@code fromIndex} to {@code toIndex} by input function
	 *
	 * @param fromIndex the initial index of the range, inclusive
	 * @param toIndex   the final index of the range, exclusive
	 * @return is answer of calculations
	 * @throws IndexOutOfBoundsException if fromIndex > toIndex or toIndex > length of input array or fromIndex < 0
	 */
	public T calc(int fromIndex, int toIndex) {
		if (fromIndex > toIndex) {
			throw new IndexOutOfBoundsException("fromIndex > toIndex");
		}
		if (toIndex > lengthInputData) {
			throw new IndexOutOfBoundsException("toIndex ( = " + toIndex + " ) > length of input array");
		}
		if (fromIndex < 0) {
			throw new IndexOutOfBoundsException("fromIndex ( = " + fromIndex + " ) < 0");
		}
		T ans = neutralElement;
		int length = values.length / 2;
		fromIndex += length;
		toIndex += length - 1;
		while (fromIndex <= toIndex) {
			if ((fromIndex & 1) == 0) {
				ans = function.apply(values[fromIndex], ans);
			}
			if ((toIndex & 1) == 1) {
				ans = function.apply(ans, values[toIndex]);
			}
			if (fromIndex == toIndex) {
				break;
			}
			fromIndex = fromIndex / 2;
			toIndex = (toIndex - 2) / 2;
		}
		return ans;
	}

	/**
	 * This method changes the element of the input array by index.
	 *
	 * @param index   is index changing element
	 * @param element is new value changing element
	 * @throws NullPointerException      if element = null
	 * @throws IndexOutOfBoundsException if index < 0 or index >= length of input array</>
	 */
	public void change(int index, T element) {
		if (element == null) {
			throw new NullPointerException("element = null");
		}
		if (index < 0) {
			throw new IndexOutOfBoundsException("index = " + index);
		}
		if (index >= lengthInputData) {
			throw new IndexOutOfBoundsException("index ( = " + index + " ) >= length of input array");
		}
		int tmp = ((values.length - 1) / 2) + index;
		values[tmp] = element;
		updateTree(tmp);
	}
}
