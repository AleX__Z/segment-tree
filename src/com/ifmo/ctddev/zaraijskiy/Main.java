package com.ifmo.ctddev.zaraijskiy;

import javax.naming.SizeLimitExceededException;

public class Main {

	public static void main(String[] args) throws SizeLimitExceededException {
		Integer[] ints = new Integer[]{-1 ,-1 ,2 ,3 ,4 ,-1, 7 ,8};
		SegmentTree<Integer> integerSegmentTree = new SegmentTree<>(ints, (x, y) ->  x == -1 ? y : x, -1);
		//integerSegmentTree.change(2 ,4);
		System.out.println(integerSegmentTree.calc (0, 7));
	}
}
