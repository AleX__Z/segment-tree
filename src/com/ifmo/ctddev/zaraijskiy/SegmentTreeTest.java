package com.ifmo.ctddev.zaraijskiy;

import org.junit.Assert;

import java.util.Arrays;

import static org.junit.Assert.*;

public class SegmentTreeTest extends Assert {

	@org.junit.Test
	public void testCalc() throws Exception {
		for (int j  = 0;j < 300;j++) {
			Integer[] integers = new Integer[1<<((int) (Math.random() * 20))];
			for (int i = 0; i < integers.length; i++) {
				integers[i] = i+1;
			}
			SegmentTree<Integer> tree  = new SegmentTree<>(integers,Math::max,0);
			int random = (int) (Math.random() * integers.length);
			//System.out.println(j);
			assertEquals((int)tree.calc(0,random),random);
		}


	}

	@org.junit.Test
	public void testChange() throws Exception {
		Integer[] integers = new Integer[1<<((int) (Math.random() * 20))];
		Arrays.fill(integers,0);
		SegmentTree<Integer> tree  = new SegmentTree<>(integers,Math::max,0);
		for (int i = 0; i <integers.length; i++) {
			tree.change(i,i+1);

		}
		for (int j  = 0;j < 300;j++) {

			for (int i = 0; i < integers.length; i++) {
				integers[i] = i+1;
			}
			int random = (int) (Math.random() * integers.length);
			//System.out.println(j);
			assertEquals((int)tree.calc(0,random),random);
		}
	}
}